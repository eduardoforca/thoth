#!/usr/bin/env python3
"""
Thoth - easy source code to pdf(latex)
"""

__author__ = "eduardoforca"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
import os
import pathlib
import json
import shutil
import fnmatch
import subprocess

class TexWriter:
    @staticmethod
    def write(output, compiler='pdflatex'):
            os.chdir(output)
            # CALLS TWICE TO GENERATE TOC 
            subprocess.check_call([compiler, 'main.tex'])
            subprocess.check_call([compiler, 'main.tex'])
        
class FileMover:
    def __init__(self):
        pass
    @staticmethod
    def include_patterns(*patterns):
        """Factory function that can be used with copytree() ignore parameter.

        Arguments define a sequence of glob-style patterns
        that are used to specify what files to NOT ignore.
        Creates and returns a function that determines this for each directory
        in the file hierarchy rooted at the source directory when used with
        shutil.copytree().

        Source: https://stackoverflow.com/questions/35155382/copying-specific-files-to-a-new-folder-while-maintaining-the-original-subdirect
        """
        def _ignore_patterns(path, names):
            keep = set(name for pattern in patterns
                                for name in fnmatch.filter(names, pattern))
            ignore = set(name for name in names
                            if name not in keep and not os.path.isdir(os.path.join(path, name)))
            return ignore
        return _ignore_patterns
    
    @staticmethod
    def move_templates(src, dest, *patterns):
        shutil.copytree(src, dest, dirs_exist_ok=True, ignore=FileMover.include_patterns('*.tex', '*.sty'))

class Thoth:
    def __init__(self, config, root=None, output="", pdf=False):
        self.config = config
        self.root = root
        self.output = output
        self.pdf = pdf
        self.temp_buffer = []
        self.main_buffer = []

    ## CHECKING METHODS
    def check_ignored_folders(self, dir):
        ignored = False
        for f in self.config.get('ignoreFolders', []):
            if dir.match(f):
                ignored = True; break
        return ignored

    def check_language_patterns(self, file, language):
        matched = False
        for f in language.get('patterns', []):
            if file.match(f):
                matched = True
                for f in language.get('ignoreFiles', []):
                    if file.match(f):
                        matched = False; break
                break
        return matched   

    ## PARSING METHODS
    def add_to_buffer(self, line):
        ''' Adds latex line to the buffer that will be written to the file
        '''
        self.main_buffer.append(line + "\n")

    def parse_group(self, language=None):
        ''' Parses a group using the groupBy from the config file. If group by language, the root tree is parsed once by language '''

        #Adds either the display name of the language, the language actual name or the root directory name
        group = language.get('display', language.get('language')) if language else self.root.stem
        self.add_to_buffer(self.latex_group(group))
        
        self.parse_tree(self.root, language)
        
        self.add_to_buffer("\end{thoth_group}")
    
    def parse_tree(self, path, language = None):
        '''Parses a directory tree recursively adding the files to the buffer to be generated
        '''
        if self.check_ignored_folders(path):
            return
        self.temp_buffer.append(self.latex_dir(path))
        for child in sorted(path.iterdir(), key=lambda x: x.suffix, reverse=True):
            if child.is_dir():
                self.parse_tree(child, language)
            else:
                if language is not None:
                    if self.check_language_patterns(child, language):
                        self.latex_file(child, language)
                else:
                    for lang in self.config.get('languages'):
                        if self.check_language_patterns(child, lang):
                            self.latex_file(child, lang)
                            break
        self.add_to_buffer("\end{directory}") if len(self.temp_buffer) == 0 else self.temp_buffer.pop()

    def parse(self):
        '''Generates the latex of a root directory according to the configuration.
        '''

        #Initial checks
        assert(len(self.config.get('languages', 0)) > 0), "Config file must contain at least one language"
        if self.output.exists():
            assert(self.output.is_dir()), "Output must be a directory"
        else:
            self.output.mkdir()
        
        #Moves the tex folder to output
        FileMover.move_templates(pathlib.Path('./tex'), self.output, '*.tex', '*.sty')
        generated = self.output / 'generated.tex'

        #Generates the .tex file to reflect the source code
        if self.config.get('groupBy', 'dir') == 'language':
            [self.parse_group(language) for language in self.config.get('languages')]                
        else:
            self.parse_group()

        #Writes to generated file
        tex = generated.open('w')
        tex.writelines(self.main_buffer)
        tex.close()

    ##WRITING METHODS
    def latex_file(self, source, language):
        # Flushes temporary buffer
        [self.add_to_buffer(x) for x in self.temp_buffer]
        self.temp_buffer.clear()
        
        string = "\sourcefile{{{}}}{{{}}}{{{}}}".format(language.get('language', ''),
                                                        language.get('style', 'default_style'),
                                                        source.name)
        self.add_to_buffer(string)

    def latex_dir(self, dir):
        relative_path = os.path.relpath(dir.resolve(), self.output.resolve())
        full_path = os.path.relpath(dir.resolve(), self.root.resolve())

        string = "\\begin{{directory}}{{{}/}}{{{}/}}".format(relative_path, full_path)
        return string

    def latex_group(self, group):
        string = "\\begin{{thoth_group}}{{{}}}".format(group)
        return string

    

def main(args):
    """ Main entry point of the app """
    config = json.load(args.config)
    root = args.root
    output = args.output.joinpath('output/')
    args.config.close()

    print("PARSING FILES FROM" + str(root))
    Thoth(config, root, output).parse()
    print("Latex Generation complete")

    if args.pdf:
        print("Compiling to pdf with " + args.compiler)
        try:
            TexWriter.write(output, args.compiler)
        except:
            print("Failed to compile PDF, but the files were generated. Try compiling it manually")
    


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Path to source code
    parser.add_argument("root", type=pathlib.Path, help="Path to source code")
    parser.add_argument("-c", "--config", dest="config", type=argparse.FileType('r'), default="config.json", help="Configuration file", required=False)
    parser.add_argument("-o", "--output", dest="output", type=pathlib.Path, default="./", help="Output root directory", required=False)
    parser.add_argument("-p", "--pdf", dest="pdf", action='store_true', help="Compile to pdf (requires pdflatex)", required=False)
    parser.add_argument("-t", "--texcompiler", dest="compiler", type=str, default="pdflatex", help="PDF compiler to use", required=False)

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    main(args)