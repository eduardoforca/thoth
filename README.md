# thoth

## Description
Thoth is a lightweight tool for parsing a source code file tree and generating LaTeX code for pdf generation. By using LaTeX, it becomes highly customizable and robust.

## Installation
You only need Python to generate the LaTeX code, but if you want to compile your PDF, you need to install a LaTeX engine.

<!-- OVERLEAF -->

## Usage
`python thoth.py <root> [--config] [--output] [--pdf] [--texcompiler]`

You need to specify the root of the project you want to generate the LaTeX code for. There is a `config.json` file included with a sample configuration. You can modify it to your needs or you can provide another one through the `config` option. By default, the generated files are written to the `output` directory (be careful, it will overwrite existing files), but you can also specify a different destination. If you want Thoth to compile the generated files to pdf, you can set the `pdf` parameter to true and also pass your desired `texcompiler` (defaults to `pdflatex`).

## Customization
Customization must be done through the template files found inside the `\tex` directory. The code highlighting is done through the `listings` package. You should add the custom styles you create to the `config` file in order to associate them with the languages in your code.

## Known Issues
* Since the generated files keep referencing the source code, any change to them will also change your LaTeX project. If you need to keep snapshots of your code, you should compile it to PDF so it won't be changed. In the future there will be an option to copy the contents of your files to the output.

## [LICENSE](LICENSE)
